const gHead = {
	viewport: document.querySelector('meta[name=viewport]'),
	contents: {
		mobile: 'width=640, user-scalable=no',
		tablet: 'width=device-width, user-scalable=no'
	},
	activeViewport: null,
	handler () {
		if (screen.width > 640 && gHead.viewport.content != gHead.contents.tablet) {
			gHead.setViewport(gHead.contents.tablet)
			gHead.activeViewport = 'tablet'
		} else if (screen.width <= 640 && gHead.viewport.content != gHead.contents.mobile) {
			gHead.setViewport(gHead.contents.mobile)
			gHead.activeViewport = 'mobile'
		}
	},
	setViewport (content) {
		gHead.viewport.content = content
	},
	init () {
		window.addEventListener('resize', gHead.handler)
		window.addEventListener('load', gHead.handler)
	}
}

module.exports = gHead
