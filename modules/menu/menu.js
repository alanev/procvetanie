const menu = {
    nav: document.querySelector('.nav'),
    links: document.querySelectorAll('.menu.menu--main .menu__link'),
    back: document.querySelector('.nav__back'),
    menu: document.querySelector('.nav__main'),
    submenu: document.querySelector('.nav__submenu'),
    toggle () {
        var className = '_submenu',
            classList = this.nav.classList,
            style = this.menu.style;
        
        if (classList.contains(className)) {
            classList.remove(className)
            style.height = ''
        } else {
            classList.add(className)
            style.height = this.submenu.offsetHeight + 'px';
        }
    },
    init () {
        if (this.links && this.links.length > 0 && this.nav && this.back && this.menu && this.submenu) {
            this.links.forEach(el => {
                el.on('click', e => {
                    this.toggle()
                })
            })
            this.back.on('click', e => {
                this.toggle()
            })
        }
    }
}
module.exports = menu;