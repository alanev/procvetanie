const noUiSlider = require('nouislider');

module.exports = {
    elements: document.querySelectorAll('.slider'),
    init () {
        if (this.elements) {
            this.elements.forEach(element => {
                let bar = element.querySelector('.slider__bar'),
                    fields = element.querySelectorAll('.slider__input');

                noUiSlider.create(bar, {
                    start: [
                        parseInt(fields[0].value),
                        parseInt(fields[1].value)
                    ],
                    step: 10,
                    range: {
                        'min': parseInt(fields[0].min),
                        'max': parseInt(fields[1].max)
                    },
																				format: {
																					to (value) {
																						return value
																					},
																					from (value) {
																						return value
																					}
																				}
                });

                bar.noUiSlider.on('update', (values, handle) => {
                    fields[handle].value = values[handle];
                });
                fields.forEach(field => {
                    field.on('change', e => {
                        bar.noUiSlider.set([
                            parseInt(fields[0].value),
                            parseInt(fields[1].value)
                        ]);
                    });
                })
            });
        }
    }
}
