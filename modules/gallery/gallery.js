module.exports = {
    image: document.querySelector('.gallery__image'),
    previews: document.querySelectorAll('.gallery__preview'),
    toggle (index) {
        this.previews.item(this.active).classList.remove('_active');
        this.active = index;
        let current = this.previews.item(this.active);
        current.classList.add('_active');
        this.image.src = current.dataset.image;
    },
    active: 0,
    init () {
        if (this.image) {
            let self = this;
            this.toggle(this.active);
            this.previews.forEach((preview, index) => {
                preview.addEventListener('click', e => {
                    self.toggle(index);
                })
            });
        }
    }
}