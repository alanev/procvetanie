const menu = {
    opener: document.querySelector('.header__menu'),
    nav: document.querySelector('.nav'),
    closer: document.querySelector('.nav__close'),
				page: document.querySelector('.g-page'),
    toggle (method = 'toggle') {
        // if (method === 'add') {
        //     window.scrollTo(0, 0)
        // }
        menu.nav.classList[method]('_active')
        menu.page.classList[method]('_nav')
    },
    init () {
        if (this.opener && this.nav && this.closer) {
            this.opener.on('click', e => {
                this.toggle('add')
            })
            this.closer.on('click', e => {
                this.toggle('remove')
            })
            window.addEventListener('hashchange', e => {
                let {hash} = location
                if (hash === '') {
                    this.toggle('remove')
                } else if (hash === '#nav') {
                    this.toggle('add')
                }
            })
            document.addEventListener('touchstart', e => {
                this.x = e.touches[0].clientX;
            })
            document.addEventListener('touchmove', e => {
                if (this.x) {
                    let x = e.touches[0].clientX
                    if (this.x < 50 && x > menu.x && x - menu.x > 100) {
                        this.toggle('add')
                    }
                    if (this.x > 525 && menu.x > x && menu.x - x > 100) {
                        this.toggle('remove')
                    }
                }
            })
}
}
}
module.exports = menu;
