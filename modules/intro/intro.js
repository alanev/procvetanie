const Flickity = require('flickity/dist/flickity.pkgd.min.js');

module.exports = {
    wrapper: document.querySelector('.intro'),
    init () {
        if (this.wrapper) {
            this.carousel = new Flickity(this.wrapper, {
                cellSelector: '.intro__slide',
                prevNextButtons: false,
                setGallerySize: false,
                contain: true,
                wrapAround: true
            });
        }
    }
}