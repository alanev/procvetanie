NodeList.prototype.forEach = Array.prototype.forEach;
Element.prototype.on = Element.prototype.addEventListener;

const nav = require('nav/nav.js');
const menu = require('menu/menu.js');
const intro = require('intro/intro.js');
const slider = require('slider/slider.js');
const gHead = require('g-head/g-head.js');
const gallery = require('gallery/gallery.js');

nav.init();
menu.init();
intro.init();
gHead.init();
slider.init();
gallery.init();
